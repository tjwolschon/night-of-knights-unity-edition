﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    Animator anim;
    float jumpHeight = -.4f;
    float downAccel = 0f;
    GameObject cam;
    SpriteRenderer sprite;


    // Use this for initialization
    void Start () {
        cam = GameObject.Find("Camera");//store the camera, so we can move it smoothly later
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {

        //Debug.Log(anim.GetBool("IsWalking"));
        //Debug.Log(anim.GetCurrentAnimatorStateInfo(0).nameHash);
        bool isGrounded;
        if(transform.position.y <= -3.5)
        {
            isGrounded = true;
            transform.position = new Vector3(transform.position.x, -3.5f);
            downAccel = 0;
        }
        else
        {
            isGrounded = false;
            downAccel += .02f;
        }


        //Get user input
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
        {
            if (Input.GetKey(KeyCode.D))
            {
                transform.position = new Vector3(transform.position.x + .1f, transform.position.y);
                cam.transform.position = new Vector3(transform.position.x + 4, 0, -3f);
                anim.SetBool("IsWalking", true);
                anim.SetBool("WalkLeft", false);
                sprite.flipX = false;
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.position = new Vector3(transform.position.x - .1f, transform.position.y);
                cam.transform.position = new Vector3(transform.position.x + 4, 0, -3f);
                anim.SetBool("IsWalking", true);
                anim.SetBool("WalkLeft", true);
                sprite.flipX = true;
            }
        }
        else
        {
            anim.SetBool("IsWalking", false);
        }
        if (Input.GetKey(KeyCode.W) && isGrounded)
        {
            downAccel = jumpHeight;
        }

        transform.position = new Vector3(transform.position.x, transform.position.y - downAccel);




        //Debug.Log(isGrounded);
        //Debug.Log(downAccel);
        //Debug.Log(jumpHeight);

    }
}
